// Write a program that prints the numbers from 1 to 100. 
// But for multiples of three print "Fizz" instead of the number 
// and for the multiples of five print "Buzz". 
// For numbers which are multiples of both three and five print "FizzBuzz".

// For Starters just write a program that prints the numbers from 1 to 100
// Do it in this file below these comments:

// If you don't have an environment set up yet thats okay. you can use http://repl.it/languages/JavaScript
// Then cut and paste the code here.

// We should use the normal branch -> commit -> pull request method
